var Balance = Balance || (function () {
    // Constructor.
    function Balance(ctor) {
        ctor = ctor || {};

        this.getUrl = ctor.getUrl || '';
        this.Params = ctor.Params || '';

    }
    //
    Balance.prototype.Init = function () {
        //Start View
        var context = this;
        var loader = AjaxLoader.show(this.container);
        Ajax.execGet(this.getUrl, this.Params, function(result)
        {
            console.log(result);
            loader.hide();
            PageLoader.loadPart("account-balance-block", "api/account_balance", result, true, function () {
            });
        });

    };
    //

    return Balance;
})();


