var BalanceParams = {
    getUrl : "api/getInfo",
    Params : ""
};
var RateParams = {
    getUrl : "api/bestRates",
    Params : ""
};
var GlassParams = {
    getUrl : "api/depth",
    rateInfo : "api/rateInfo",
    firstCurrency : "",
    secondCurrency  : "",
    limit : 50
};
var TradeParams = {
    getUrl : "api/trade",
    firstCurrency : "",
    secondCurrency : "",
    type : "",
    rate : "",
    amount : ""
};
var FeeParams = {
    getUrl : "api/fee",
    firstCurrency : "",
    secondCurrency : "",
    feePaste : ""
};
var OrdersParams = {
    getUrl : "api/activeOrders",
    cancelUrl : "api/cancelOrder",
    firstCurrency : "",
    secondCurrency : "",
    cancelOrderId : ""
};
var TradeHistoryParams = {
    getUrl : "api/tradeHistory",
    firstCurrency : "",
    secondCurrency : "",
    count : 50
};
var glass;
var trade;
var fee;
var balance;
var rate;
var orders;
var tradeHistory;
$(document).ready(function () {
balance = new Balance(BalanceParams);
    balance.Init();
rate = new Rate(RateParams);
    rate.Init();
});