var Trade = Trade || (function () {
    // Constructor.
    function Trade(ctor) {
        ctor = ctor || {};

        this.getUrl = ctor.getUrl || '';
        this.firstCurrency = ctor.firstCurrency || '';
        this.secondCurrency = ctor.secondCurrency || '';
        this.type = ctor.type || '';
        this.rate = ctor.rate || '';
        this.amount = ctor.amount || '';

    }
    //
    Trade.prototype.Init = function () {
        //Start View
        var loader = AjaxLoader.show(this.container);
        var Params = jQuery.parseJSON('{ "firstCurrency": "'+this.firstCurrency+'","secondCurrency": "'+this.secondCurrency+'", "type": "'+this.type+ '","rate": "'+this.rate+'","amount": "'+this.amount+'"}');
        console.log(Params);
        Ajax.execGet(this.getUrl, Params, function(result)
        {
            loader.hide();
            console.log(result);
            if (result.success == 0)
                alert(result.error);
            if (result.success == 1)
            {
                balance.Init();
                $(".choosedWidget > .dashboard-button").click();
            }
        });


    };
    //

    return Trade;
})();