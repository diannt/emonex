var Orders = Orders || (function () {
    // Constructor.
    function Orders(ctor) {
        ctor = ctor || {};

        this.getUrl = ctor.getUrl || '';
        this.cancelUrl = ctor.cancelUrl || '';
        this.firstCurrency = ctor.firstCurrency || '';
        this.secondCurrency = ctor.secondCurrency || '';
        this.cancelOrderId = ctor.cancelOrderId || '';

    }
    //
    Orders.prototype.Init = function () {
        //Start View
        var context = this;
        var loader = AjaxLoader.show(this.container);
        var Params = jQuery.parseJSON('{ "firstCurrency" : "'+this.firstCurrency+'","secondCurrency": "'+this.secondCurrency+'" }');
        Ajax.execGet(this.getUrl, Params, function(result)
        {
            result = jQuery.parseJSON('{"Data":'+JSON.stringify(result)+', "Curr": { "firstCurrency": "'+Params.firstCurrency+'","secondCurrency": "'+Params.secondCurrency+'"}}');
            loader.hide();
            PageLoader.loadPart("widgetOrders", "api/widget_orders", result, true, function () {
            });
        });

    };

    Orders.prototype.Cancel = function() {

        var Params = jQuery.parseJSON('{ "order_id" : "'+this.cancelOrderId+'" }');
        Ajax.execGet(this.cancelUrl, Params, function(result)
        {

        });

    };
    //

    return Orders;
})();


