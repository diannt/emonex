var Rate = Rate || (function () {
    // Constructor.
    function Rate(ctor) {
        ctor = ctor || {};

        this.getUrl = ctor.getUrl || '';
        this.Params = ctor.Params || '';

    }
    //
    Rate.prototype.Init = function () {
        //Start View
        var context = this;
        var loader = AjaxLoader.show(this.container);
        var a = 0;
        $.each($(".dashboard-button"),function(i, entry){
            if ($(entry).closest('div').hasClass('choosedWidget')){
                a = i;
                return;
            }
        });
        Ajax.execGet(this.getUrl, this.Params, function(result)
        {
                loader.hide();
                if (result.data!="")
                {
                    PageLoader.loadPart("widgetrate", "api/widget_rate", result, true, function () {
                            $(".dashboard-button")[a].click();
                      });
                }
                else
                    $("#widgetrate").html("We have sold everything! Try again later!");
        });

    };
    //

    return Rate;
})();


