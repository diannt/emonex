var Glass = Glass || (function () {
    // Constructor.
    function Glass(ctor) {
        ctor = ctor || {};

        this.getUrl = ctor.getUrl || '';
        this.rateInfo = ctor.rateInfo || '';
        this.firstCurrency = ctor.firstCurrency || '';
        this.secondCurrency = ctor.secondCurrency || '';
        this.limit = ctor.limit || '';
    }
    //
    Glass.prototype.Init = function () {
        //Start View
        var context = this;
        var loader = AjaxLoader.show(this.container);
        var Params = jQuery.parseJSON('{ "firstCurrency" : "'+this.firstCurrency+'","secondCurrency": "'+this.secondCurrency+'" }');
        var a;

        Ajax.execGet(this.rateInfo, Params, function(result)
        {
            console.log(result);
            a = result;
        });
        Params = jQuery.parseJSON('{ "firstCurrency": "'+this.firstCurrency+'","secondCurrency": "'+this.secondCurrency+'", "limit": "'+this.limit+ '"}');

        Ajax.execGet(this.getUrl, Params, function(result)
        {
            loader.hide();
			result = jQuery.parseJSON('{"Data":'+JSON.stringify(result)+', "Curr": { "firstCurrency": "'+Params.firstCurrency+'","secondCurrency": "'+Params.secondCurrency+'"}}');
            console.log(result);
            PageLoader.loadPart("widgetGlass", "api/widget_glass", result, true, function () {
                $("#max_price").html(a.bid);
                $("#min_price").html(a.ask);
                $("#b_price").val(a.ask);
                $("#s_price").val(a.bid);
                var c = $("#curname1").html();
                var d = $("#curname2").html();
                if ($("#"+c).html()!="undefined")
                {
                    var e = $("#val"+c).html();
                    $("#cur1").html(e);
                }
                if ($("#"+d).html()!="undefined")
                {
                    var f = $("#val"+d).html();
                    $("#cur2").html(f);
                }
            });
        });
    };
    //
    return Glass;
})();