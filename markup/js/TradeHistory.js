var TradeHistory = TradeHistory || (function () {
    // Constructor.
    function TradeHistory(ctor) {
        ctor = ctor || {};

        this.getUrl = ctor.getUrl || '';
        this.firstCurrency = ctor.firstCurrency || '';
        this.secondCurrency = ctor.secondCurrency || '';
        this.count = ctor.count || '';
    }
    //
    TradeHistory.prototype.Init = function () {
        //Start View
        var context = this;
        var loader = AjaxLoader.show(this.container);
        var Params = jQuery.parseJSON('{ "firstCurrency" : "'+this.firstCurrency+'","secondCurrency": "'+this.secondCurrency+'","count":"'+this.count+'"}');
        console.log(Params);
        Ajax.execGet(this.getUrl, Params, function(result)
        {
            loader.hide();
            result = jQuery.parseJSON('{"Data":'+JSON.stringify(result)+', "Curr": { "firstCurrency": "'+Params.firstCurrency+'","secondCurrency": "'+Params.secondCurrency+'"}}');
            PageLoader.loadPart("widgetTradeHistory", "api/widget_tradehistory", result, true, function () {
            });
        });

    };
    //

    return TradeHistory;
})();


