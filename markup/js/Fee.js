var Fee = Fee || (function () {
    // Constructor.
    function Fee(ctor) {
        ctor = ctor || {};

        this.getUrl = ctor.getUrl || '';
        this.firstCurrency = ctor.firstCurrency || '';
        this.secondCurrency = ctor.secondCurrency || '';
        this.feePaste = ctor.feePaste || '';

    }
    //
    Fee.prototype.get = function () {
        //Start View
        var context = this;
        var loader = AjaxLoader.show(this.container);
        var Params = jQuery.parseJSON('{ "firstCurrency": "'+this.firstCurrency+'","secondCurrency": "'+this.secondCurrency+'"}');
        console.log(Params);
        Ajax.execGet(this.getUrl, Params, function(result)
        {
            loader.hide();
            console.log(result);
            if (this.feePaste == 'b_fee')
            {
                var a = $("#b_btc").val();
                $("#b_fee").html = a*result.trade;
            }

            if (this.feePaste == 's_fee')
            {
                var b = $("#s_btc").val();
                $("#s_fee").html = b*result.trade;
            }
        });


    };
    //

    return Fee;
})();